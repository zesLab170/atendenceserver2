var mongoose = require('mongoose')


var Profile = mongoose.model('Profile',{
  name:{
    type: String,
    minlength: 2,
    trim: true
  },
  mobile:{
    type: String
  },
  fb_ID:{
    type: String
  },
  fb_Token:{
    type: String
  },
  officeLocation:{
    type:String
  }
});

module.exports={Profile}

var mongoose = require('mongoose')

/////////********  mongoose model
var Admin = mongoose.model('Admin',{
  username:{
    type: String,
    trim:true,
    require:true
  },
  password:{
    type:String
  },
  IsLogin:{
    type: Boolean
  },
  lastLoginTime:{
    type: Date,
    default: Date.now 
  }
});

module.exports={Admin}

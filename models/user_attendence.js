var mongoose = require('mongoose')

/////////********  mongoose schema
var schema = mongoose.Schema({
User_Id:{
  type:String
},
 username:{
    type: String
},
in_Time:{
  type:String
},
in_Date:{
  type:String
},
out_Time:{
  type:String
},
out_Date:{
  type:String
},
mMonth:{
  type:String
},
mYear:{
  type:String
},
mDate:{
  type:String
},
Location_on_InTime:{
  type: String
},
Location_on_OutTime:{
  type:String
},
action_to_perform:{
  type:String
},
total_Time:{
  type:Number,
  default:0
},
officeLat:{
    type:Number
},
officeLong:{
    type:Number
}
});

///////////////   creating model
var Attendence = mongoose.model('Attendence', schema);

module.exports={Attendence}

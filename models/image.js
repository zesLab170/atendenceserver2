var mongoose = require('mongoose')


/////////********  mongoose model
var Image = mongoose.model('Image',{
  ads_name:{
    type: String
  },
  ads_path:{
   type:String
  },
  ads_Id:{
  	type:String
  },
  ads_view:{
    type:Number
  },
  counter:{
  	type:Number
  },
  created:{
  	type:Date,
  	default:Date.now
  }
});

module.exports={Image}

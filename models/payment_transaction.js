var mongoose = require('mongoose')


/////////********  mongoose model
var Payment = mongoose.model('Payment',{
  pStatus:{
    type:String
  },
  pEmail:{
    type: String
  },
  pPhone:{
   type:String
  },
  pTransaction:{
  	type:String
  },
  pAmount:{
  	type:String
  },
  pGroupname:{
    type:String
  },
  created:{
  	type:Date,
  	default:Date.now
  }
});

module.exports={Payment}

var mongoose = require('mongoose')

/////////********  mongoose model
var Group = mongoose.model('Group',{
  groupname:{
    type: String,
    minlength: 2,
    trim: true
  },
  Mvisible:{
    type:Boolean
  }
});

module.exports={Group}

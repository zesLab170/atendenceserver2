const express = require('express')
var bodyParser = require('body-parser')
var morgan = require('morgan')
var dateFormat = require('dateformat');
var fs = require('fs');
// var sync = require('sync');
// var promise =  require('promise');
const path = require('path')
const ejs = require('ejs')
var multer = require('multer');
var geolib = require('geolib');
var {mongoose}= require('./db/mongoose')
var moment_timezone = require('moment-timezone')

/////////////////////////////         for server openshift 
// var upload = multer({dest: process.env.OPENSHIFT_DATA_DIR}).single('myImage');

// console.log('getting the url of dir'+process.env.OPENSHIFT_DATA_DIR);

// console.log('getting the url of dir REPO'+process.env.OPENSHIFT_REPO_DIR);


//// set storage engine
const storage = multer.diskStorage({
  destination:'./public/uploads',
  filename: function(req,file,cb){
    cb(null,file.fieldname + '-' +Date.now() +
      path.extname(file.originalname));
  }
});

// Init upload
const upload = multer({
  storage: storage
}).single('myImage');



var app = express()
app.set('view engine', 'ejs')

 ////////// middelware morgan
 app.use(morgan('dev'))

 app.use(express.static(__dirname + '/public'))

 var {Users} = require('./models/user')
 var {Admin} = require('./models/admin')
 var {Image} = require('./models/image')
 var {Payment} = require('./models/payment_transaction')
 var {Attendence} = require('./models/user_attendence')
 var {WorkDays} = require('./models/user_work_days')
 var {savingDailyAttendenceFunction} = require('./dbutil')
 var {saveDayCountFunction} = require('./dbutil')
 var {makeUserEnterIntoOffice} = require('./userEnersInOffice')
 var {makeUserRejectFromOffice} = require('./userEnersInOffice')
 var {increasedays} = require('./dbutil')
 var {signupFunction} = require('./dbutil')
 var {Company} = require('./models/company_profile')
 var {Group} = require('./models/group')

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
// alternate use of body-parser
app.use(bodyParser.json());
var newdate = new Date();

////////  getting dirname
// console.log("getting dir name"+__dirname);
// var cdate = dateFormat(newdate,"d/m/yyyy")

// var mydate = newdate.timezone('Asia/Calcutta').format('Ha z');
// // // console.log('mydate is :  '+mydate);
// console.log("kolkata"+moment_timezone().tz("Asia/Calcutta").format());
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("L"))
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("D"))
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("M"))
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("YYYY"))
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("hh:mm:ss"))
// console.log('dateformat'+moment_timezone().tz("Asia/Calcutta").format("DD/MM/YYYY")) 
// console.log("cn/central"+moment_timezone().tz("Canada/Central").format("L"));
// console.log("cn/Atlantic"+moment_timezone().tz("Canada/Atlantic").format());
// console.log("america/Los_Angeles"+moment_timezone().tz("America/Los_Angeles").format());

///////   function for getting date and time
// function getMyDate(){

  // var in_isoTime = dateFormat(now,"isoTime")
  // console.log(in_isoTime);
  // var in_Date = dateFormat(now,"shortDate")
  // console.log(in_Date);
  // var out_isoTime = dateFormat(now, "isoTime")
  // console.log(out_isoTime);
  // var out_Date = dateFormat(now, "shortDate")
  // console.log(out_Date);
// }
// app.get('/zesadmins',zesadmins);



// app.post('/adsupload',upload.single('openshiftimage'),function(req,res){

// });
/////////////////  have to try this one
// app.use('/adsupload', express.static(process.env.OPENSHIFT_DATA_DIR+'/adsupload'));

///////////////    SAVING ADMIN SIGNUP  ////////////
function save_admin(admin,res){
  admin.save(function(err,todos){
    if(err){
      console.log('error occured while while saving admin')
      res.json({result:false,msg:'error while saving the admin',err:err})
    }else if(todos){
      console.log('saving admin')
      res.json({result:true,Data:todos})

    }
  })

}
///////////////    SAVING ADMIN SIGNUP  /////////////

////////////      ADMIN SIGNUP   ///////////////////
function admin_signup(req,res){
  var admin = new Admin({
    username: req.body.username,
    password: req.body.password
  })
  var query = {username:req.body.username};
  Admin.findOne(query,function(err,todos){
    if(err){
      console.log('error occured'+err);
      res.json({result:false,msg:'error while finding the admin'})
    }else if(!todos){
      console.log('Data not found of admin');
      save_admin(admin,res);
    }else if(todos){
      console.log('user found');
      res.json({result:false,msg:'admin already exists'})
    }
  })
}
////////////      ADMIN SIGNUP   ///////////////////

//////////        ADMIN LOGIN    /////////////////////
function admin_login(req,res){
  var admin = new Admin({
    username: req.body.username,
    password: req.body.password
  })
  var query = {username:req.body.username,password:req.body.password};
  Admin.findOne(query,function(err,todos){
    if(err){
      console.log('error occured'+err);
      res.json({result:false,msg:'error while finding the admin'})
    }else if(!todos){
      console.log('Data not found of admin');
      res.json({result:false,msg:'admin not  exists'})
    }else if(todos){
      console.log('user found');
      res.json({result:true,Data:todos})
    }

})
}

//////////        ADMIN LOGIN    /////////////////////

//////////        ADMIN PANEL ROUTES       /////////////////////

//////////        ADMIN PANEL ROUTES       /////////////////////

//////////////  refuund payment funtions
function refund_payment(req,res){
  Payment.findOne({pGroupname:req.body.groupname,pEmail:req.body.email},function(err,refund){
    if(err){
      console.log("error while finding the transaction_id"+ err);
      res.json({result:false,msg:err})
    }else if(!refund){
        console.log("no transaction_id found");
        res.json({result:false,msg:"no transaction_id found"})
    }else if(refund){
          console.log("refund found");
          res.json({result:true,Data:refund})

    }
  })
}
////////////  payment detail saving
function paymentTransaction(req,res){
  var payment = new Payment({
    pStatus: req.body.status,
    pEmail: req.body.email,
    pPhone: req.body.phone,
    pAmount: req.body.amount,
    pTransaction: req.body.transaction_id,
    pGroupname: req.body.groupname
  })

  console.log('payment detail'+req.body.amount);
  console.log('payment detail'+payment)
   payment.save(function(err){
          if(err){
             console.log('error while saving payment in database'+err)
             res.json({result:false,msg:"error in saving payment detail err:"+err})
          }
          console.log('payment saved in database')
          res.json({result:true,Data:true})
        })
}

//////////////////  desplaying ads upload
// app.get('/adsupload', function(req, res) {
//  res.render('imageupload');
// });
// var userAgent = System.getProperty( "http.agent" );
// console.log('hey @@@@@ are you :'+ userAgent);



////////  post route for upload images
app.post('/adsupload',(req,res)=>{
   // console.log("detail before saving"+req.file)
  upload(req, res, (err) =>{
    if(err){
      res.render('imageupload',{
        msg: err
      });
    } else if(req.file== undefined){
     res.render('imageupload',{
      msg:'Error: No file selected'
     });
    }else{
      console.log("detail before saving"+req.file)
      // var dirName = process.env.OPENSHIFT_DATA_DIR;
      // console.log('dirname'+dirName)
      // console.log('dirname2'+OPENSHIFT_DATA_DIR);
      
      var ads = new Ads({
        ads_name: req.file.filename,
        ads_path: path.join(__dirname+'./public/uploads/'+req.file.filename)
      })
      ads.save().then((doc)=>{
    // res.send(doc)
    console.log('image saved in database')
  },(e)=>{
    // req.status(400).send(e)
    console.log('error while saving image in database'+err)
  });
      console.log('getting filename'+req.file.filename);
      res.render('imageupload',{
        msg: 'File Uploaded',
        file: 'uploads/'+req.file.filename
      });
    }
  })
})

///////////   for generating random number

// console.log(

//     Math.floor(1000 + Math.random() * 9000)

// );

////////////////////   image upload system
function uploadADS(req,res){
  console.log('getting u')
  var imagename = req.body.imagename;
  var image = new Image({
        ads_Id:    Math.floor(1000 + Math.random() * 9000),
        ads_name: imagename,
        ads_path: req.body.imageurl,
        ads_view: 0,
        counter: 0
      })

        image.save(function(err){
          if(err){
             console.log('error while saving image in database'+err)
             res.json({result:false,msg:"error uploading err:"+err})
          }
          console.log('image saved in database')
          res.json({result:true,Data:""+imagename+" uploaded sucessfull"})
        })
  //     ads.save().then((doc)=>{
  //   // res.send(doc)
  //   console.log('image saved in database')
  //    return res.json({result:true,Data:""+req.body.filename+" uploaded sucessfull"})

  // },(e)=>{
  //   // req.status(400).send(e)
  //   console.log('error while saving image in database'+err)
  //   return res.json({result:false,msg:""+req.body.filename+" uploaded failed"})
  // }); 
}

function callIfValidLoggedIn(req, res, callback)
{
  // check if user is logged in 

    callback(req,res);
}
///// function for sleep
function sleep(){
  var ms =60;
  var date = new Date();

  console.log("GOING sleep for 1 min"+ date.getTime());
  var sleep = require('system-sleep');
  sleep(ms*1000);
  console.log("sleep for 1 min DONE"+ date.getTime());
}
/*          login and signupp functions    */
// ///////////////   function for signup
// function signup(req, res) {
//   var user = new Users({
//     username: req.body.username,
//     mobile: req.body.mobile,
//     email: req.body.email,
//     password: req.body.password,
//     managerId: req.body.managerId,
//     roleId: req.body.roleId,
//     companyName: req.body.companyName
//   });

//   user.save().then((doc)=>{
//     res.send(doc)
//     console.log(doc)
//   },(e)=>{
//     res.status(400).send(e);
//   });
//   console.log(res.body);
// }

// var ran = Math.random();
//  var count = Image.count({},function(err, count) {
//            console.log('Count is ' + count);
//            var random = Math.random()%count;
//   console.log('geting random'+Math.round(random));
//    console.log('getting count'+count);
          
//       });
var random;
// console.log('getting count'+count(random));

/////////////////    count function 
function count(random){
  Image.count({},function(err, count) {
    if(err){
      console.log('error while counting'+err);
      res.json({result:false,msg:'error while counting'})
    }else if(!count){
       console.log('no count');
      res.json({result:false,msg:'no count'})
    }else if(count){


           console.log('Count is ' + count);
          random = Math.random()%count;
  console.log('geting random'+Math.round(random));
   console.log('getting count'+count);
   return random;
 }
          
      });
}
  
/////////////////   display one image at one time
function displayoneImage(req,res){
   var update = {$inc: {ads_view: 1}};
   var count = Image.count({});
  var random = Math.random()%count;
  console.log('geting random'+random);
 Image.find({},function(err,docs){
  if(err){
    console.log('error while the getting images');
    return res.json({result:false,msg:"error"+err})
  }else if(!docs){
    console.log(' images not found');
    return res.json({result:false,msg:"image not found"})
  }else if(docs){
     console.log(' images  found');
      // res.json({result:true,Data:docs})
          Image.count({},function(err, count) {
    if(err){
      console.log('error while counting'+err);
      res.json({result:false,msg:'error while counting'})
    }else if(!count){
       console.log('no count');
      res.json({result:false,msg:'no count'})
    }else if(count){

           // console.log('Count is ' + count);
        var  random = Math.random()%count;
  // console.log('geting random'+Math.round(random));
   // console.log('getting count'+count);
   var round = Math.round(random);
   // console.log('doc result'+docs[round].ads_path);
   res.json({result:true,Data:docs[round].ads_path})
 }
          
      });
   
  }
 })
  
 
}
////////////////////  show all images
function showAllImages(req,res){
  check_admins(req,res,displayAllImages)
}

/////////////////    function that will display images
function displayAllImages(req,res){

    Image.find({},function(err,display){
      if(err){
        console.log('err to display images')
        return res.status(500).send({msg:'error while displaying images'})
      }else if(!display){
        console.log('image not found');
        return res.status(404).send({msg:'image not found'})
      }else if(display){
        console.log('image found')
        return res.status(200).send(display)
      }
    })
}

/////////////////  SHOW USER ATTENDANCE
function showUserAttendance(req,res){
  check_admins(req,res,dumpuserAttendance);
}
///////////////////   get user attendance
function dumpuserAttendance(req,res){
  Attendence.find({username:req.body.username},function(err,todos){
    if(err){
      console.log('error while finding attendence '+err)
      return res.json({result:false,msg:e})
    }
    else if(!todos){
      console.log('no atteendance found')
     return res.json({result:false,msg:"data not found"})
   }
   if(todos){
    console.log('attendence found')
    return res.json({result:true,Data:todos});
   }
  })
}

////////////////   funcction to find user exist or not and save it
function findUsername(req,res){
  var user = new Users({
    username: req.body.username,
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password,
    managerId: req.body.managerId,
    roleId: req.body.roleId,
    groupName: req.body.groupName,
    userAgent: req.body.userAgent
  });
  Users.findOne({username:req.body.username},function(err,userfound){
        if(err){
          console.log(err)
          return res.status(500).send('error while signup'+err)
        }
        if(!userfound){
          console.log("============================");
          console.log(' this is unique username : ' + req.body.username)
          signupFunction(user,res);
        }
        if(userfound){
          res.json({result:false,data:"user already exist"});
        }
      })// END HERE USER.FINDONE//

}
////////////////   function for restrict user to signup if allready exist
function SignUp(req, res){
  
          findUsername(req,res);
     
}

////////////////////// function groupname
function creategroup(req, res){
  
Users.findOne({groupName:req.body.groupName},function(err,groupFound){
        if(err){
          console.log('error while fnding group name:'+err)
          return res.json({result:false,msg:err});
        }
        else if(!groupFound){
          console.log('group name not found :'+req.body.groupName);
         Users.findOneAndUpdate({username:req.body.username},{$set:{groupName:req.body.groupName,roleId:"2"}},{new:true},
    function(err,name){
      if(err){
        console.log('error while setting groupname'+ err);
        return res.json({result:false,msg:err});
      }else if(!name){
        console.log('username not found @@'+req.body.groupName);
        ///////////////  
        return res.json({result:false,msg:"username not found"})
      }else if(name){
        console.log('username found wow');
        return res.json({result:true,Data:name})
      }
    }
    )
        
        }
        else if(groupFound){
          console.log('group found')
          return res.json({result:false,msg:"groupname already exist"})
        }
      })
/////////////////////
 

}

/////////////////////////////   route function for selecting groupname
function joingroup(req,res){
   var user = new Users({
    username: req.body.username,
    roleId: req.body.roleId,
    groupName: req.body.groupName
  });

console.log('joingroup')
      Users.findOne({groupName:req.body.groupName},function(err,groupFound){
        if(err){
          console.log('error while fnding group name:'+err)
          return res.json({result:false,msg:err});
        }
        else if(!groupFound){
          console.log('group name not found :'+req.body.groupName);
         return res.json({result:false,msg:"groupname not found"});
        
        }
        else if(groupFound){
          if(req.body.groupName=="TEST"){
             Users.findOneAndUpdate({username:req.body.username},{$set:{groupName:req.body.groupName}},{new:true},
    function(err,name){
      if(err){
        console.log('error while setting groupname'+ err);
        return res.json({result:false,msg:err});
      }else if(!name){
        console.log('username not found @@'+req.body.groupName);
        ///////////////   i wil write later
        return res.json({result:false,msg:"username not found"})
      }else if(name){
        console.log('username found wow');
        return res.json({result:true,Data:name})
      }
    }
    )
           }else{


          Users.count({groupName:req.body.groupName},function(err,count2){
            if(err){
      console.log('error while counting'+err);
      res.json({result:false,msg:'error while counting'})
    }else if(!count2){
       console.log('no count');
      res.json({result:false,msg:'no count'})
    }else if(count2<5){

          console.log('group found')
          console.log('count is less than 6')
           Users.findOneAndUpdate({username:req.body.username},{$set:{groupName:req.body.groupName}},{new:true},
    function(err,name){
      if(err){
        console.log('error while setting groupname'+ err);
        return res.json({result:false,msg:err});
      }else if(!name){
        console.log('username not found @@'+req.body.groupName);
        ///////////////   i wil write later
        return res.json({result:false,msg:"username not found"})
      }else if(name){
        console.log('username found wow');
        return res.json({result:true,Data:name})
      }
    }
    )
               } ///////// end of if(count<6)
               ////////  have to check payment 
               else if(count2>4){
                    console.log('count is MORE than 6')
                    Payment.findOne({pStatus:true,pGroupname:req.body.groupName},function(err,payment){
                      if(err){
                        console.log(err);
                        return res.json({result:false,msg:err});
                        }
                      if(!payment){
                        return res.json({result:false,msg:"payment not found"});
                        }if(payment){
                           Users.findOneAndUpdate({username:req.body.username},{$set:{groupName:req.body.groupName}},{new:true},
                            function(err,name){
                              if(err){
                                console.log('error while setting groupname'+ err);
                                return res.json({result:false,msg:err});
                              }else if(!name){
                                console.log('username not found @@'+req.body.groupName);
                                ///////////////   i wil write later
                                return res.json({result:false,msg:"username not found"})
                              }else if(name){
                                console.log('username found wow');
                                return res.json({result:true,Data:name})
                              }
                            }
                            )
                        }
                    })
               }
          }) 
          } 

        }
      })

    
}

////////// function for login
function login(req,res){
  var mUsername = req.body.username;
  var mPassword = req.body.password;
  Users.findOne({username:mUsername,password:mPassword}, function(err, user){
    if(err){
      console.log(err);
      return res.json({result:false,msg:err});
    }
    if(!user){
      return res.json({result:false,msg:"user not found"});
    }
    // modigyLastLogin(req,res,mUsername)
    return res.json({result:true,Data:user});
  })
}

////////////////  function for facebook login
function facebook(req, res){
  var email = req.body.email;
  var users = new Users({
    username: req.body.username,
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password,
    groupName: req.body.groupName
   
  });
  console.log('what we get::'+users);
  Users.findOne({email:email}, function(err, user){
    if(err){
      console.log(err);
      return res.json({result:false,Data:err});
    }
    if(!user){
     console.log("new user from facebook signup");
     console.log(req.body.email);
     return signupFunction(users,res);
   }
   console.log("returing facebook user logged in");
   console.log(user);
   return res.json({result:true,Data:user});

 })
}

///////////////////// function for show user by managerid
function userUnderManager(req, res){
  Users.findOne({_id:req.body._id}, function(err,todos){
    if(err){
      console.log('error while finding userid'+err);
      return res.json({result:false,msg:err});
    }
    else if(!todos){
      console.log('user not found');
      return res.json({result:false,msg:'user not found'})
    }
    else if(todos){
      console.log('user found');
      return res.json({result:true,Data:todos.subordinateId})
    }
  })
  // Users.find({username:req.body.username}).then(todos =>{
  //   res.json({Data: todos})
  //   next()
  // })
  // .catch(e =>{
  //   res.json({msg:e});
  // })
}

///////////////////////   show my manager 
function showMymanagerInternal(req,res)
{
    Users.find({roleId:req.body.roleId}).then(todos =>{
    res.json({result:true,Data:todos})
  })
  .catch(e=>{
    res.json({result:false,msg:e});
  })

}
function showMyManager(req, res){
  Users.find({roleId:req.body.roleId}).then(todos =>{
    res.json({result:true,Data:todos})
  })
  .catch(e=>{
    res.json({result:false,msg:e});
  })
  // callIfLoggedIn(req,res,showMymanagerInternal);
 }


//////////////////  setting subordinte via suordinate request  /////
function Subordinate_added_to_manager(req,res){
  console.log("i am here")
  var is_subuser_found="not found";
var have_toupdate = "not now";
  var query = {username:req.body.managerId},
      update = {
          $push:{subordinateId:req.body.username}
               },
    options = {upsert: true,'new':true};
    Users.findOneAndUpdate(query, update, options, function(err, todos){
      if(err){
        return  res.json({result:false,msg:e});
      }
      else if(!todos){
       return res.json({result:false,msg:"user not found"});
      }
      else if(todos){
console.log('yappy subordinate setting sucessfull')
        return res.json({result:true,Data:todos});
      }
    })
}
//////////////////  setting subordinte via suordinate request  /////
 /////////////     set my manager finally
 function setManagerInternally(req,res,userFound1){
  
   userFound1.managerId=req.body.managerId
  
  userFound1.save().then((doc)=>{
    res.json({result:true,Date:doc})     
        console.log(doc);
           console.log("           save succesfull your manager");
    },(e)=>{
      res.json({result:false,msg:e});
  })

 }

///////////////////////    set my manager  
function setMyManager(req,res){
Users.findOne({username:req.body.managerId,groupName:req.body.groupName},function(err,managerfound){
  if(err){
    console.log("err: "+ err)
    return res.json({result:false,msg:err})
  }else if(!managerfound){
    console.log("manager not found")
    return res.json({result:false,msg:"manager not found"})
  }else if(managerfound){
    console.log("manager found")
    existingSubUser(req,res,userfoundfunction)
  }
})
}
//////////////////// set my subordinate
// function setMySubordinate(req, res){

// checkCompany(req,res,setSubordinate);
  
// }
// /////////////////// callback function that will check same company and allow to add or not
// function checkCompany(req,res,setSubordinate){
  
// }

// ////////////////  check user exist or not 
// function checkUser(req,res){
//   Users.findOne({username:username},function(err,userFound){
//     if(err){
//       console.log("error while finding username and error is :"+ err)
//       return res.json({result:false,msg:err})
//     }
//     if(!userFound){
//       console.log("user not found");
//       return res.json({result:false,msg:"user not found"})
//     }
//     console.log('user found')
//   })
// }
/////////////////////////   existing subordinate
/**
* send me user_Id and unsername
*/
function existingSubUser(req,res,callback){
  var query_for_existing_subUser = {username:req.body.managerId};
  Users.findOne(query_for_existing_subUser,function(err,userFound1){
    if(err){
      ///// error 
      console.log('getting error in existinguser function'+err)
      return res.json({result:false,msg:err});
    }else if(!userFound1){
      // user not found
      console.log('user not found :'+req.body.managerId);
      return res.json({result:false,msg:"user not found"+req.body.managerId});
    }else if(userFound1){
      // user found
      console.log('user found'+req.body.managerId);
      callback(req,res,userFound1);
     // abc(req,res,userFound1);
       
    }
  })
}
//////////////////////// route of existing user
app.post('/eusers',existingUser);
/**
* send me user_Id and unsername
*/
function existingUser(req,res,callback){
  var query_for_existing_subUser = {username:req.body.username};
  Users.findOne(query_for_existing_subUser,function(err,userFound1){
    if(err){
      ///// error 
      console.log('getting error in existinguser function'+err)
      return res.json({result:false,msg:err});
    }else if(!userFound1){
      // user not found
      console.log('user not found :'+req.body.username);
      return res.json({result:false,msg:"user not found"+req.body.username});
    }else if(userFound1){
      // user found
      console.log('user found'+req.body.username);
      callback(req,res,userFound1);
     // abc(req,res,userFound1);
       
    }
  })
}
///////////////  function callback for update user with subordinate
function userfoundfunction(req,res,userFound1){
console.log('letme check'+userFound1.subordinateId[0])
console.log('userFound1 log'+ userFound1.subordinateId)
var is_subuser_found="not found";
var have_toupdate = "not now";
var subordinatesize = userFound1.subordinateId.length;
console.log('subordinatesize'+subordinatesize)
var i = 0;
    if(subordinatesize>0 && subordinatesize != undefined){ 
    console.log('checking size') 
      for(i=0;i<subordinatesize;i++){
        if(req.body.username==userFound1.subordinateId[i]){
          console.log(req.body.username+'=='+userFound1.subordinateId[i])
        is_subuser_found="found";
        }else{
          have_toupdate= "yes";
        }
      }
    }else{
      Subordinate_added_to_manager(req,res);  
    }
    if(is_subuser_found=="found"){
       // 
       return res.json({result:false,msg:'user already exist'});
    }else if(have_toupdate=="yes"){
      Subordinate_added_to_manager(req,res);  
    }
   
    
}
///////////////  update manager profile with subordinate detail
function setMySubordinateInternal(req,res){
  console.log("i am here")
  var query = {_id:req.body.User_Id},
      update = {
          $set:{roleId:"2"},
          $push:{subordinateId:req.body.subordinateId}
               },
    options = {upsert: true,'new':true};
    Users.findOneAndUpdate(query, update, options, function(err, todos){
      if(err){
        return  res.json({result:false,msg:e});
      }
      else if(!todos){
       return res.json({result:false,msg:"user not found"});
      }
      else if(todos){
console.log('yappy subordinate setting sucessfull')
        return res.json({result:true,Data:todos});
      }
    })
}

/////////////// set subordinate if all test pass
function setMySubordinate(req,res){
  var query1 = {username:req.body.subordinateId,groupName:req.body.groupName};

Users.findOne(query1, function(err,subuserfound){
    if(err){
      console.log("error while finding subordinate and error is :"+ err)
      return res.json({result:false,msg:err})
    }
    if(!subuserfound){
      console.log("user that you are searching to add in my subordinate is not found");
      return res.json({result:false,msg:"subordinate user not found"})
    }
    else if(subuserfound){
      ///// sending for checking the manager(user) exist or not
      existingUser(req,res,userfoundfunction);
  console.log("user found now you can add in your subordinate")
  
  }
})//////// user function find one end
}
//  Users.update({_id:req.body.User_Id},{$push:{subordinateId:req.body.subordinateId}}).then(todos =>{
//      res.json({result:true,Data:todos})
//   })
//   .catch(e=>{
//     res.json({result:false,msg:e})
//   })
// })

////////////////////   setting user's office location function
function setOfficeLoc(req, res){
  var userid = req.body.User_Id;
  var officelocation = req.body.officelocation;
  Users.findOneAndUpdate({_id:userid}, {$set:{officeLat:req.body.officeLat,officeLong:req.body.officeLong}}, {new: true},
   function(err, location){
    if(err){
      console.log(err);
      return res.status(500).send(err);
    }
    if(!location){
     console.log(req.body.User_Id);
     return res.send(404).send("user id not found");
   }
   console.log(location);
   return res.status(200).send(location);

 })
}

//////////////  function for checking the distance between them
// 
function checkDistancefromOffice(req,isnearstatus,res,callUserNear, callUserFaR){
  console.log("calling function of distance for ====>"+ req);
  console.log("calling function of distance for user id "+ req.body.User_Id);
  Users.findOne({_id:req.body.User_Id},  function(err, location){
    if(err){
      console.log("some error we will return")
      console.log(err);
      return res.status(500).send(err);
    }
    if(!location){
      console.log("some error we will return 22222")
      console.log(req.body.User_Id);
      return res.send(404).send();
    }
    console.log(location.officeLat);
    console.log(location.officeLong);

    console.log(req.body.officeLat);
    console.log(req.body.officeLong);

    var dfo = geolib.getDistance(
      {latitude: req.body.officeLat, longitude: req.body.officeLong},
      {latitude: location.officeLat, longitude: location.officeLong}
      ); 
    console.log("in the function of checking distance called");
    if(dfo<=50){
     isnearstatus = true;
    
     console.log("isnearstatus changed to true")

     callUserNear(req,isnearstatus,res);
   }else{
    isnearstatus = false;
        callUserFaR(req,isnearstatus,res,dfo);
        console.log("isnearstatus changed to false")
  }
  console.log("distance from office "+ dfo);

      // return res.status(200).send(location);
    })
  // return ;
}

///////////////////  show all attendance
function showAllattendance(req,res){
  check_admins(req,res,dumpAttendenceTable)
}

////////////// dumpAttendenceTable
function dumpAttendenceTable(req,res){
  Attendence.find({},function(err,todos){
    if(err){
      console.log('error while finding attendence '+err)
      return res.json({result:false,msg:e})
    }
    else if(!todos){
      console.log('no attendance found')
     return res.json({result:false,msg:"data not found"})
   }
   if(todos){
    console.log('attendence found')
    return res.json({result:true,Data:todos});
   }
  })
}

///////////////////  function for deleting user as well as attandance
function deleteuserAndAttendance(req,res){
  Users.findOne({username:req.body.username}, function(err,user){
    if(err){
       console.log('error while finding username')
      return res.json({result:false,msg:err});

    }else if(!user){
      console.log('user not found '+req.body.username);
        return res.json({result:false,msg:""+req.body.username+" not found"})

    }else if(user){
      console.log('user you want to delete'+req.body.username);
      Users.remove({username:req.body.username},function(err,user){
    if(err){
      console.log('error while finding username')
      return res.json({result:false,msg:err});
    }if(!user){
      console.log('user not found '+req.body.username);
        return res.json({result:false,msg:"user not found"})
    }if(user){
    Attendence.remove({username:req.body.username},function(err,attendance){
      if(err){
        console.log('error while deleting '+req.body.username+'from attendance table');
        return res.json({result:false,msg:err})
      }if(attendance){
        console.log('deleted');
         return res.json({result:true,Data:""+req.body.username+" is deleted"})
      }
     
    })
  }
  })
    }
  })
}

////////////  checking valid admins  
function check_admins(req,res,callback){
   var query8 = {username:req.body.username,password:req.body.password};
   Admin.findOne(query8,function(err,todos){
    if(err){
      console.log('error while finding the admins');
        return res.json({result:false,msg:'error while finding admins'})
    }if(!todos){
      console.log('admin not found');
        return res.json({result:false,msg:'admin not found'})
    }else if(todos){
       console.log('admin  found');
        callback(req,res);
        // return res.redirect('/users');
    }
   })
}

/////////////  show all users
function showAllUsers(req,res){
  check_admins(req,res,sendAllUserData)
}

//////////   function for showing users table data
function sendAllUserData (req, res){
  Users.find().then(tasks => {
    res.json({result:true,Data:tasks})
  })
  .catch(err => {
    console.log('error'+err)
    res.json({result:false,msg:"error while finding users",err:err})
  })
};

///////////////////////////   function user/month
function UserMonthData(req,res){
  console.log('data from app'+req.body)
  var username = req.body.username;
  var month = req.body.mMonth;
  Attendence.find({username:username,mMonth:month}, function(err, todos){
    if(err) {
            // return console.log(err);
            return res.status(500).json({message: err.message});
          }
console.log('data from server '+todos)
        res.json({Data: todos}); //2nd todos = the array import above
      });
}

///////////////////////////   function user/totalworkday
function UserTotalWorkDay(req,res){
  var ui = req.body.User_Id;
  WorkDays.find({User_Id:ui}, function(err, todos){
    if(err) {
            // return console.log(err);
            return res.status(500).json({message: err.message});
          }

        res.json({Data: todos}); //2nd todos = the array import above
      });
}

//////////////// removing dublicate files
function remove_dublicate_files(user,date,month,year){
  var queryForAttendance = Attendence.findOne({username:user})
                                   .where('mDate').equals(date)
                                   .where('mMonth').equals(month)
                                   .where('mYear').equals(year)
       queryForAttendance.exec(function (err, attendence0){
        if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }else if(!attendence0){
          console.log("---- attendence not found");
        }else if(attendence0){
           Attendence.remove({ _id: attendence0._id }, function(err) {
                        console.log('dublicate removed')
                      })
        }
      })

}
//////////////  function for calling how many days he work in month
function howManyDaysInMonth(req,res){
  var month = req.body.mMonth;
  var user_remove,date_remove,month_remove,year_remove;
  var find_Date = Attendence.find({username:req.body.username})
                                   .where('mMonth').equals(req.body.mMonth)
                                   .where('mYear').equals(req.body.mYear)
                                   .select('mDate')
  find_Date.exec(function(err,tasks){
    if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }else if(tasks){
           console.log('getting how many days he worked in months'+ tasks.length);
      console.log('getting how many days he worked in months'+ req.body.username);
      console.log('getting how many days he worked in months'+ req.body.mMonth);
      console.log('getting how many days he worked in months'+ req.body.mYear);
       res.json({result:true,Days:tasks,Month:month})
        }
   
      // for(var i = 0;i<tasks.length;i++){
      //   console.log('tasks[i] *** logs'+tasks[i].mDate)
      //   for(var j = 1;j<tasks.length;j++){
      //       if(tasks[i].mDate == tasks[j].mDate){

      //       user_remove   = req.body.username;
      //       date_remove   = tasks[j].mDate;
      //       month_remove  = tasks[j].mMonth;
      //       year_remove   = tasks[j].mYear;

      //       remove_dublicate_files(user_remove,date_remove,month_remove,year_remove);
      //    }
      //   }
        
      // }
      
   
  })
  
}
//////////////////// function for calling how many days he work in year
function howManyDaysInYear(req,res){
  Attendence.find({username:req.body.username,mYear:req.body.mYear}).then(tasks =>{
    console.log('getting how many days he worked in year'+tasks.length);
    res.json({result:true,Data:tasks.length})
  })
  .catch(err =>{
    console.log('error while getting userid,month'+err)
      res.json({result:false,msg:err})
  })
}
//////////////   function for showing usersWorkingHours
function usersWorkingHours (req, res, next){
  Attendence.find().then(tasks => {
    res.send(200, tasks)
    next()
  })
  .catch(err=>{
    res.send(500,err)
  })
}

/////// function for getting specific user attendence
function specific_user_attendence(req,res){
  var userid= req.body.User_Id;
  console.log(userid);
  Attendence.find({User_Id:userid}, function(err, todos){
    if(err) {
              // return console.log(err);
              return res.status(500).json({message: err.message});
            }

          res.json({Data: todos}); //2nd todos = the array import above
        });
}

//// POST method user_data for user collenctions
app.post('/userdata', function (req, res) {
  var user = new Users({
    name: req.body.name,
    mobile: req.body.mobile,
    email: req.body.email,
    password: req.body.password,
    fb_ID: req.body.fb_ID,
    fb_Token: req.body.fb_token
  });
  user.save().then((doc)=>{
    res.send(doc)
  },(e)=>{
    req.status(400).send(e)
  });
  console.log(req.body);
});


////////  POST method for  users attendence
app.post('/attendence', function (req, res) {

  var now = new Date();

  var attendence = new Attendence({
    User_Id: req.body.User_Id,
    username:req.body.username,
    mYear: dateFormat(now, "yyyy"),
    mMonth: dateFormat(now, "m"),
    Location_on_InTime: req.body.Location_InTime,
    Location_on_OutTime: req.body.Location_on_OutTime,
    action_to_perform: req.body.action_to_perform
  });

  var workdays = new WorkDays({
    User_Id: req.body.User_Id,
    total_work_days: 1,
    current_Month:dateFormat(now, "m"),
    current_Date: dateFormat(now, "dd/mm/yyyy")
  })


  if(req.body.action_to_perform=='InTime'){
    var isnearstatus = false;

    checkDistancefromOffice(req,isnearstatus,res,makeUserEnterIntoOffice, makeUserRejectFromOffice);

}

else if(req.body.action_to_perform=='OutTime'){
  console.log('req is ::::'+req.body);
  var id = req.body.id;
  var usertimezone = req.body.timezone;
  var now2 = new Date();
  var queryForAttendance = Attendence.findOne({User_Id:req.body.User_Id})
                                   .where('mDate').equals(moment_timezone().tz(usertimezone).format("D"))
                                   .where('mMonth').equals(moment_timezone().tz(usertimezone).format("M"))
                                   .where('mYear').equals(moment_timezone().tz(usertimezone).format("YYYY"));

  queryForAttendance.exec(function (err, attendence2){
        if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }
        else if(!attendence2){
          console.log('attendence2 not found ')
          // savingDailyAttendenceFunction(attendence,res);
          // have to create new one
        }
        else if(attendence2){
     var vUser_Id   =   attendence2.User_Id;
     var vYear      =   attendence2.mYear;
     var vMonth     =   attendence2.mMonth;
     var vDate      =   attendence2.in_Date;
     var vTime      =   attendence2.in_Time;
     var inLocation =   attendence2.Location_on_InTime;
     attendence.User_Id            = vUser_Id;
     attendence.username           = attendence2.username;
     attendence.mYear              = vYear;
     attendence.mMonth             = vMonth;
     attendence.mDate              = attendence2.mDate;
     attendence.in_Date            = vDate;
     attendence.in_Time            = vTime;
     console.log("intime:"+vTime);
     attendence.Location_on_InTime = inLocation;
     attendence.out_Time = moment_timezone().tz(usertimezone).format("hh:mm:ss");
     attendence.out_Date = moment_timezone().tz(usertimezone).format("DD/MM/YYYY");
     var intime = attendence.in_Time;
     var outtime = attendence.out_Time;
     console.log('==============================')

     console.log('intime'+ intime)

     console.log('outtime'+ outtime)
     console.log('==============================')
     var inhours = Number(intime.substring(0,2)) + Number(intime.substring(3,5))/60 + Number(intime.substring(6,8))/3600;
     console.log(inhours);
     var outhours = Number(outtime.substring(0,2)) + Number(outtime.substring(3,5))/60 + Number(outtime.substring(6,8))/3600;
     console.log(outhours);

     console.log("we are going to add total time: " + attendence2.total_Time);
     attendence.total_Time = attendence2.total_Time + (outhours - inhours) ;

         ////  delete
         Attendence.remove({ _id: attendence2._id }, function(err) {
           savingDailyAttendenceFunction(attendence,res)
         });
       }
     })
}
  // console.log(req.body);
});




// POST /login gets urlencoded bodies
// app.post('/SignUp', urlencodedParser, signup )
/////////////// set my subordinate
app.post('/setMyManager',setMyManager);

/////////////// set my subordinate
app.post('/setMySubordinate', setMySubordinate);

///////////////  signup via app
app.post('/signupIfNew',SignUp);
///////////////  create groupname
app.post('/creategroup',creategroup);

///////////////  join groupname
app.post('/joingroup',joingroup);

//////////////////////   Login method
app.post('/login',login);

////////////////////////    facebook login
app.post('/login/facebook',facebook);

//////////////////////////  post for saving user's office locations
app.post('/setofficeLoc',setOfficeLoc);

/////////////////  find user under manager
app.post('/users/manager',userUnderManager);
//////// find method for finding the a perticular user attendence
app.post('/SU_attendence', specific_user_attendence);

//////////////////////////   find the all working days of user
app.post('/user/totalworkdays', UserTotalWorkDay);

///////////////////// user/month
app.post('/user/month',UserMonthData);

/////////////  route for calling how many days he work in month
app.post('/month/worked', howManyDaysInMonth);

/////////////  route for calling how many days he work in year
app.post('/year/worked', howManyDaysInYear);

//////////////////// paymentTransaction
app.post('/paymentTransaction',paymentTransaction);

/////////////////// user who want to refund 
app.post('/refund/payment',refund_payment);

////////////// delete user and his all attendence
app.post('/deleteuserAndAttendance',deleteuserAndAttendance);

///////////   test for update methods
app.post('/update1',function(req,res){
  Users.findOneAndUpdate({_id:req.body.User_Id}, {$set:{name:"bhaskar"}}, {new: true}, function(err, doc){
    if(err){
      console.log("Something wrong when updating data!");
    }

    console.log(doc);
    return res.status(200).send(doc)
  });
})

///////////////  ADMIN SIGNUP  
app.post('/admin/signup',admin_signup);

////////////// ADMIN LOGIN
app.post('/admin/login',admin_login);

/////////// dumpAttendenceTable
app.post('/dumpAttendenceTable',showAllattendance);

//////////////////   get methods
app.post('/users', showAllUsers);


// app.get('/usersWorkingHours',usersWorkingHours);

//////////////////////   display all images (DAI)
app.post('/displayAllImages',displayAllImages);

/////////////////////////////   display one image
app.get('/displayoneImage',displayoneImage);

//////////////////  get all attendence of user
app.post('/dumpuserAttendance',dumpuserAttendance);


///////////////////   image upload system
app.post('/uploadADS',uploadADS);


// app.get('/', (req, res) =>{
//   res.writeHead(302, {
//     'Location': '/form.html'
//   //add other headers here...
// });
//   res.end();
// });
// var  now4 = new Date();
// // console.log(dateFormat(now4,"dd/mm/yyyy"));

app.listen(3000, ()=>
{
  console.log('server is up and running on port 3000');
 });


// Object.assign=require('object-assign')

// app.engine('html', require('ejs').renderFile); 
// app.use(morgan('combined'))

// var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
//     ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
//     mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
//     mongoURLLabel = "";

// if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {
//   var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase(),
//       mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'],
//       mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'],
//       mongoDatabase = process.env[mongoServiceName + '_DATABASE'],
//       mongoPassword = process.env[mongoServiceName + '_PASSWORD']
//       mongoUser = process.env[mongoServiceName + '_USER'];

//   if (mongoHost && mongoPort && mongoDatabase) {
//     mongoURLLabel = mongoURL = 'mongodb://';
//     if (mongoUser && mongoPassword) {
//       mongoURL += mongoUser + ':' + mongoPassword + '@';
//     }
//     ///// printjing the mongodb url
//     console.log(mongoURLLabel);
//     // Provide UI label that excludes user id and pw
//     mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
//     mongoURL += mongoHost + ':' +  mongoPort + '/' + mongoDatabase;

//   }
// }
// var db = null,
//     dbDetails = new Object();

// var initDb = function(callback) {
//   if (mongoURL == null) return;

//   var mongodb = require('mongodb');
//   if (mongodb == null) return;

//   mongodb.connect(mongoURL, function(err, conn) {
//     if (err) {
//       callback(err);
//       return;
//     }

//     db = conn;
//     dbDetails.databaseName = db.databaseName;
//     dbDetails.url = mongoURLLabel;
//     dbDetails.type = 'MongoDB';

//     console.log('Connected to MongoDB at: %s', mongoURL);
//   });
// };

//      // // POST /login gets urlencoded bodies
//   // app.post('/data', urlencodedParser, function (req, res) {
//      //   var user = new Users({
//      //     text: req.body.name,
//      //     mobile: req.body.mobile,
//      //     email: req.body.email,
//      //     password: req.body.password
//      //   });
//      //   user.save().then((doc)=>{
//      //       res.send(doc)
//      //   },(e)=>{
//      //     req.status(400).send(e)
//      //   });
//      //   console.log(req.body);
//      // });

// ////////////   to show my database
// app.get('/users', sendAllUserData);

// app.get('/', function (req, res) {
//   // try to initialize the db on every request if it's not already
//   // initialized.
//   if (!db) {
//     initDb(function(err){});
//   }
//   if (db) {
//     var col = db.collection('counts');
//     // Create a document with request IP and current time of request
//     col.insert({ip: req.ip, date: Date.now()});
//     col.count(function(err, count){
//       if (err) {
//         console.log('Error running count. Message:\n'+err);
//       }
//       res.render('index.html', { pageCountMessage : count, dbInfo: dbDetails });
//     });
//   } else {
//     res.render('index.html', { pageCountMessage : null});
//   }
// });

// app.get('/pagecount', function (req, res) {
//   // try to initialize the db on every request if it's not already
//   // initialized.
//   if (!db) {
//     initDb(function(err){});
//   }
//   if (db) {
//     db.collection('counts').count(function(err, count ){
//       res.send('{ pageCount: ' + count + '}');
//     });
//   } else {
//     res.send('{ pageCount: -1 }');
//   }
// });

// // error handling
// app.use(function(err, req, res, next){
//   console.error(err.stack);
//   res.status(500).send('Something bad happened!');
// });

// initDb(function(err){
//   console.log('Error connecting to Mongo. Message:\n'+err);
// });

// app.listen(port, ip);
// console.log('Server running on http://%s:%s', ip, port);

// module.exports = app ;


var mongoose = require('mongoose');
var dateFormat = require('dateformat');
 var {Users} = require('./models/user')
 var {Attendence} = require('./models/user_attendence')
 var {WorkDays} = require('./models/user_work_days')
 var {increasedays} = require('./dbutil')
 var {savingDailyAttendenceFunction} = require('./dbutil')
 var {saveDayCountFunction} = require('./dbutil')
 var moment_timezone = require('moment-timezone')

 //////////////// monment logs

// console.log("kolkata"+moment_timezone().tz("Asia/Kolkata").format());
// console.log("cn/central"+moment_timezone().tz("Canada/Central").format());
// console.log("cn/Atlantic"+moment_timezone().tz("Canada/Atlantic").format());
// console.log("america/Los_Angeles"+moment_timezone().tz("America/Los_Angeles").format());
 
 // var now = new Date();
///////////////////////    ********  queries  ***********  ///////////////////////


var makeUserRejectFromOffice = function makeUserRejectFromOffice(req,isnearstatus,res,distance)
{
	console.log("User is outside office distance"+ distance);
	res.json({result:true,nearoffice:false});
	return;	
}
var makeUserEnterIntoOffice = function makeUserEnterIntoOffice2222(req,isnearstatus,res){
  var now = new Date();
  var usertimezone = req.body.timezone;

    var attendence = new Attendence({
    User_Id: req.body.User_Id,
    username:req.body.username,
    mYear: moment_timezone().tz(usertimezone).format("YYYY"),
    mMonth: moment_timezone().tz(usertimezone).format("M"),
    mDate: moment_timezone().tz(usertimezone).format("D"),
    in_Time: moment_timezone().tz(usertimezone).format("hh:mm:ss"),
    Location_on_InTime: req.body.Location_InTime,
    Location_on_OutTime: req.body.Location_on_OutTime,
    action_to_perform: req.body.action_to_perform
  });
  var id = req.body.id;
  var indate    = req.body.in_Date;
  console.log("indate= "+indate  +" "+ moment_timezone().tz(usertimezone).format("DD/MM/YYYY"));
  if(indate == moment_timezone().tz(usertimezone).format("DD/MM/YYYY")){
    
    var queryForAttendance = Attendence.findOne({User_Id:req.body.User_Id})
                                   .where('mDate').equals(moment_timezone().tz(usertimezone).format("D"))
                                   .where('mMonth').equals(moment_timezone().tz(usertimezone).format("M"))
                                   .where('mYear').equals(moment_timezone().tz(usertimezone).format("YYYY"));

     console.log('app date and server date matched')
      queryForAttendance.exec(function (err, attendence0){
        if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }
        else if(!attendence0){
          console.log('attendence0 not found '+attendence0)
          savingDailyAttendenceFunction(attendence,res);
          // have to create new one
        }
        else if(attendence0){

        console.log('----- attendence0 found : '+attendence0)
  
        console.log('----- attendence0 found : '+attendence0.User_Id);
        console.log("reched");
          attendence.User_Id            = attendence0.User_Id;
          attendence.username           = attendence0.username;
          attendence.total_Time         = attendence0.total_Time;
          attendence.mYear              = attendence0.mYear;
          attendence.mMonth             = attendence0.mMonth;
          attendence.out_Date           = attendence0.out_Date;
          attendence.out_Time           = attendence0.out_Time;
          attendence.Location_on_InTime = attendence0.Location_on_InTime;
          attendence.Location_on_OutTime = attendence0.Location_on_OutTime;
          attendence.in_Time = moment_timezone().tz(usertimezone).format("hh:mm:ss");
          attendence.in_Date = moment_timezone().tz(usertimezone).format("DD/MM/YYYY");
          console.log('all ______ updated ')

                      ////  delete
                      Attendence.remove({ _id: attendence0._id }, function(err) {
                        savingDailyAttendenceFunction(attendence,res)
                        console.log('log! --- @ $ '+id)
                      })

        }
        
      })



  }
  else{
        console.log('else date doesnt match')
  }


}
var makeUserEnterIntoOfficeold = function makeUserEnterIntoOffice(req,isnearstatus,res)
{
	
	 
    var attendence = new Attendence({
    User_Id: req.body.User_Id,
    mYear: dateFormat(now, "yyyy"),
    mMonth: dateFormat(now, "m"),
    in_Time: dateFormat(now, "isoTime"),
    out_Date: dateFormat(now,"dd/mm/yyyy"),
    Location_on_InTime: req.body.Location_InTime,
    Location_on_OutTime: req.body.Location_on_OutTime,
    action_to_perform: req.body.action_to_perform
  });

  var workdays = new WorkDays({
    User_Id: req.body.User_Id,
    total_work_days: 1,
    current_Date: dateFormat(now, "dd/mm/yyyy")
  })


  var id = req.body.id;
  console.log("checking row id "+id);
  var indate    = req.body.in_Date;
  console.log("indate= "+indate  +" "+ dateFormat(now, "dd/mm/yyyy"));
  // if (id.match(/^[0-9a-fA-F]{24}$/)) {
    // console.log("id matched !!!!!!!!!!!!!" +id)
    if(indate == dateFormat(now, "dd/mm/yyyy")){

var queryForAttendance = Attendence.find({User_Id:req.body.User_Id})
                                   .where('mDate').equals(dateFormat(now,"dd"))
                                   .where('mMonth').equals(dateFormat(now,"m"))
                                   .where('mYear').equals(dateFormat(now,"yyyy"));

      console.log('app date and server date matched')
      queryForAttendance.exec(function (err, attendence0){
        if(err){
          console.log('---------  !!!!!!! err queryForAttendance'+ err)
        }
        else if(!attendence0){
          console.log('attendence0 not found ')
          savingDailyAttendenceFunction(attendence,res);
          // have to create new one

        }
        else if(attendence0){
        console.log('----- attendence0 found : '+attendence0)
///modefy 
          console.log("reched");
          attendence.User_Id            = attendence0.User_Id;
          attendence.total_Time         = attendence0.total_Time;
          attendence.mYear              = attendence0.mYear;
          attendence.mMonth             = attendence0.mMonth;
          attendence.out_Date           = attendence0.out_Date;
          attendence.out_Time           = attendence0.out_Time;
          attendence.Location_on_InTime = attendence0.Location_on_InTime;
          attendence.Location_on_OutTime = attendence0.Location_on_OutTime;
          attendence.in_Time = dateFormat(now, "isoTime");
          attendence.in_Date = dateFormat(now,"dd/mm/yyyy");

                      ////  delete
                      Attendence.remove({ _id: attendence0._id }, function(err) {
                        savingDailyAttendenceFunction(attendence,res)
                        console.log('log! --- @ $ '+id)
                      })
        }
        
      })



      // Attendence.findOne({_id: id }).exec()
      // .then(function( attendence0){
      //   if(attendence0)
      //   {
      //     console.log("reched");
      //     attendence.User_Id            = attendence0.User_Id;
      //     attendence.total_Time         = attendence0.total_Time;
      //     attendence.mYear              = attendence0.mYear;
      //     attendence.mMonth             = attendence0.mMonth;
      //     attendence.out_Date           = attendence0.out_Date;
      //     attendence.out_Time           = attendence0.out_Time;
      //     attendence.Location_on_InTime = attendence0.Location_on_InTime;
      //     attendence.Location_on_OutTime = attendence0.Location_on_OutTime;
      //     attendence.in_Time = dateFormat(now, "isoTime");
      //     attendence.in_Date = dateFormat(now,"dd/mm/yyyy");

      //                 ////  delete
      //                 Attendence.remove({ _id: id }, function(err) {
      //                   savingDailyAttendenceFunction(attendence,res)
      //                   console.log('log! --- @ $ '+id)
      //                 });
      //             }else{
      //               console.log('------!---!  else of attendence0')
      //                savingDailyAttendenceFunction(attendence,res)
      //             }

      //         })
    // }
                  ///////////////    function for increase day count
              // increasedays(indate, now, workdays,res);
            //   if(indate == dateFormat(now, "dd/mm/yyyy")){
            //     console.log(req.body.User_Id)
            //     WorkDays.findOne({User_Id: req.body.User_Id}, function(err,workday){
            //       if(err){
            //         console.log(err);
            //         return res.status(500).send();
            //       }
            //       if(!workday){
            //         console.log('new row created for workdays');
            //         saveDayCountFunction(workdays,res);
            //       }
            //       console.log('found workdays of user updating')
            //       WorkDays.findOne({User_Id: req.body.User_Id }).exec()
            //       .then(function(workdays0){
            //         if(workdays0)
            //         {
            //           if(workdays0.current_Date != dateFormat(now, "dd/mm/yyyy"))
            //           {
            //             workdays.total_work_days            = workdays0.total_work_days + 1;
            //             workdays.current_Date               = dateFormat(now, "dd/mm/yyyy");
            //             console.log(workdays0.total_work_days);
            //             console.log(workdays.total_work_days);
            //             console.log(workdays.current_Date);

            //             ////  delete
            //             WorkDays.remove({User_Id: req.body.User_Id}, function(err) {
            //               saveDayCountFunction(workdays,res)
            //             });
            //         }
            //     }
            // })
            //     })

            //   }

          }else{

            attendence.in_Time = dateFormat(now, "isoTime");
            attendence.in_Date = dateFormat(now,"dd/mm/yyyy");
            savingDailyAttendenceFunction(attendence,res)


//             if(indate == dateFormat(now, "dd/mm/yyyy")){
//               console.log(req.body.User_Id)
//               WorkDays.findOne({User_Id: req.body.User_Id}, function(err,workday){
//                 if(err){
//                   console.log(err);
//                   return res.status(500).send();
//                 }
//                 if(!workday){
//                   console.log('new row created for workdays');
//                   saveDayCountFunction(workdays,res);
//                 }
//                 console.log('found workdays of user updating')
//                 WorkDays.findOne({User_Id: req.body.User_Id }).exec()
//                 .then(function(workdays0){
//                   if(workdays0)
//                   {
//                     if(workdays0.current_Date != dateFormat(now, "dd/mm/yyyy"))
//                     {
//                       workdays.total_work_days            = workdays0.total_work_days + 1;
//                       workdays.current_Date               = dateFormat(now, "dd/mm/yyyy");
//                       console.log(workdays0.total_work_days);
//                       console.log(workdays.total_work_days);
//                       console.log(workdays.current_Date);

//             ////  delete
//             WorkDays.remove({User_Id: req.body.User_Id}, function(err) {
//               saveDayCountFunction(workdays,res)
//             });
//         }
//     }
// })
//               })

            }

            console.log("new row created");
// }  ////else of id match
}

module.exports = {makeUserEnterIntoOffice,makeUserRejectFromOffice}